#!/bin/sh
#COBALT --jobname bssolctra_$jobid
#COBALT -O $jobid
#COBALT -q debug-cache-quad
#COBALT -n 1
#COBALT -t 60
#COBALT --attrs mcdram=cache:numa=quad:enable_ssh=1
#COBALT -A BS-SOLCTRA

export ID=${COBALT_JOBID}


###cd $SLURM_SUBMIT_DIR
rm -Rf results_${ID}


##############Load needed modules##################################

#These modules are essential for BS-SOLCTRA execution
module swap PrgEnv-intel PrgEnv-gnu


####################################################################


###################################################################

# option  long version            (explanation)                                 
#                                                                               
# -n                              "PEs" (ranks)                                 
# -N      --pes-per-node          ranks per node                                
# -d      --cpus-per-pe           hyperthreads per rank                         
# -cc     --cpu-binding depth                                                   
# -j                              cpus (hyperthreads) per compute unit (core)   

###############Execution commands####################################
counter=0                                                                       
chares=32                                                                       
for ((n=0;n<2;n++))                                                             
do                                                                              
    JOBNAME="${ID}_${chares}_${counter}"                                        
    echo ${JOBNAME}                                                             
    aprun -n 2 -N 2 -d 62 -j 1  -r 1 --cc none ./bs_solctra 62 3968 10000 0.001 1 200000 300000 $JOBNAME 1 +LBDebug 1 +balancer GreedyLB +setcpuaffinity ++ppn31 +pemap 0-61 +commap 62,63
    let counter++                                                               
done    

#####################################################################

