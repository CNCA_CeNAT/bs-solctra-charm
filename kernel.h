#include <string.h>
#include <cmath>
#include <sstream>
#include "utils.h"


void load_coil_data(double* x, double* y, double* z, const std::string& path); 
void e_roof(GlobalData& data);
void printeroof(GlobalData& data, const int subsetIndex, const std::string& outputPath);
cartesian magnetic_field(Coil* rmi, Coil* rmf, const GlobalData& data, const Particle& point);
void initializeGlobals(Coil* rmi, Coil* rmf);
void finishGlobals(Coil* rmi, Coil* rmf);
bool computeIteration(const GlobalData& data, Particle& start_point, const double& step_size, const int mode, Coil* rmi, Coil* rmf, int* divergenceCounter);
void printSubsetFile(const Particle* particles, const int particleAmount, const int subsetIndex, const int iteration, const std::string& outputPath);
void printSubsetText(const Particle* particles, const int particleAmount, const int subsetIndex, const int iteration, const std::string& outputPath);
inline double norm_of(const cartesian& vec){ return sqrt(( vec.x * vec.x ) + ( vec.y * vec.y ) + ( vec.z * vec.z ));}
inline double norm_of(const Particle& vec){return sqrt(( vec.x * vec.x ) + ( vec.y * vec.y ) + ( vec.z * vec.z ));}

 


