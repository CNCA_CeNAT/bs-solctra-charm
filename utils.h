#ifndef __UTILS__
#define __UTILS__


#include <cstdio>
#include <string>
#include "pup.h"

#define PI      3.141592654
#define miu     1.2566e-06
#define I       -4350
#define MINOR_RADIUS 0.0944165
#define MAJOR_RADIUS 0.2381
#define ALIGNMENT_SIZE 64
#ifdef KNL
#define GRADES_PER_PAGE ALIGNMENT_SIZE  * KNL / sizeof(double)
#else
#define GRADES_PER_PAGE ALIGNMENT_SIZE / sizeof(double)
#endif
#define TOTAL_OF_GRADES 360
#define TOTAL_OF_GRADES_PADDED 384
#define TOTAL_OF_COILS 12



/*****Required structures****************/

struct cartesian
{
    double x, y, z;
    void print()
    {
        printf("X=[%e]. Y=[%e]. Z=[%e].\n", x, y, z);
    }
};


struct Particle
{
    double x, y, z;
};
PUPbytes(Particle);

struct Coil
{
    double* x;
    double* y;
    double* z;

};

struct GlobalData
{
    Coil coils;
    Coil e_roof;
    double* length_segment;
};

/*******************************************/

//Function prototypes
void* _mmm_malloc(size_t size, size_t alignment);
void _mmm_free(void* pointer);
void createDirectoryIfNotExists(const std::string& path);
bool directoryExists(const std::string& path);
void loadCartesianFile(double* x, double* y, double* z, const int length, const std::string& path);
std::string getZeroPadded(const int num);
void loadParticleFile(Particle* particles, const int length, const std::string& path, const int nodeIndex);
double randomGenerator(const double min, const double max, const int seedValue);
void initializeParticles(Particle * particles, const int length, const int seedValue);

#endif
