#!/bin/sh
#COBALT --jobname bssolctra_$jobid
#COBALT -O $jobid
#COBALT -q debug-cache-quad
#COBALT -n 1
#COBALT -t 60
#COBALT --attrs mcdram=cache:numa=quad:enable_ssh=1
#COBALT -A BS-SOLCTRA

export ID=${COBALT_JOBID}


###cd $SLURM_SUBMIT_DIR
rm -Rf results_${ID}


###################################################################

# option  long version            (explanation)                                 
#                                                                               
# -n                              "PEs" (ranks)                                 
# -N      --pes-per-node          ranks per node                                
# -d      --cpus-per-pe           hyperthreads per rank                         
# -cc     --cpu-binding depth                                                   
# -j                              cpus (hyperthreads) per compute unit (core)   

###############Execution commands####################################
counter=0                                                                       
chares=64                                                                       
JOBNAME="${ID}_${chares}_${counter}"                                        
echo ${JOBNAME}                                                             
aprun -n 1 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 62 3968 10000 0.001 1 200000 300000 $JOBNAME 1 +LBDebug 1 +balancer GreedyLB ++ppn62 +traceroot projections +pemap 0-61 +commap 63
#####################################################################

