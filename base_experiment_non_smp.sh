#!/bin/sh
#COBALT --jobname bssolctra_$jobid
#COBALT -O $jobid
#COBALT -q debug-cache-quad
#COBALT -n 4
#COBALT -t 60
#COBALT --attrs mcdram=cache:numa=quad:enable_ssh=1
#COBALT -A BS-SOLCTRA

export ID=${COBALT_JOBID}


###cd $SLURM_SUBMIT_DIR
rm -Rf results_${ID}


###################################################################

# option  long version            (explanation)                                 
#                                                                               
# -n                              "PEs" (ranks)                                 
# -N      --pes-per-node          ranks per node                                
# -d      --cpus-per-pe           hyperthreads per rank                         
# -cc     --cpu-binding depth                                                   
# -j                              cpus (hyperthreads) per compute unit (core)   

###############Execution commands####################################
counter=0                                                                       
chares=62
type="NON-SMP"                                                                       
for ((n=0;n<2;n++))
do
    JOBNAME="${ID}_${type}_${chares}_${counter}"                                        
    echo ${JOBNAME}                                                             
    aprun -n 248 -N 62 -d 1 -j 1  -r 1 --cc none ./bs_solctra 248 15872 10000 0.001 1 200000 300000 $JOBNAME 1 +LBDebug 1 +balancer GreedyLB +pemap 0-61
    let counter++
done


#JOBNAME="${ID}_${chares}_${counter}"                                        
#aprun -n 4 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 248 15872 10000 0.001 1 200000 300000 $JOBNAME 1 +LBDebug 1 +balancer GreedyLB +traceroot projections ++ppn62 +pemap 0-61 +commap 62
#####################################################################

