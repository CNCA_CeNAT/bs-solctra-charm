#!/bin/sh
#COBALT --jobname bssolctra_$jobid
#COBALT -O $jobid
#COBALT -q default
#COBALT -n 320
#COBALT -t 120
#COBALT --attrs mcdram=cache:numa=quad:enable_ssh=1
#COBALT -A BS-SOLCTRA

export ID=${COBALT_JOBID}


###cd $SLURM_SUBMIT_DIR
rm -Rf results_${ID}


###################################################################

# option  long version            (explanation)                                 
#                                                                               
# -n                              "PEs" (ranks)                                 
# -N      --pes-per-node          ranks per node                                
# -d      --cpus-per-pe           hyperthreads per rank                         
# -cc     --cpu-binding depth                                                   
# -j                              cpus (hyperthreads) per compute unit (core)   

###############Execution commands####################################
echo ${COBALT_PARTNAME}
echo "Starting 1:1 ratio"
counter=0                                                                       
chares=62                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 992 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done


echo "Starting 2:1 ratio"
counter=0                                                                       
chares=124                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 1984 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

echo "Starting 4:1 ratio"
counter=0                                                                       
chares=248                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 3968 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

echo "Starting 8:1 ratio"
counter=0                                                                       
chares=496                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 7936 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

echo "Starting 16:1 ratio"
counter=0                                                                       
chares=992                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 15872 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

echo "Starting 32:1 ratio"
counter=0                                                                       
chares=1984                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 31744 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

echo "Starting 64:1 ratio"
counter=0                                                                       
chares=3968                                                                       
runType="SMP"
for ((n=0;n<10;n++))
do
    JOBNAME="${ID}_${runType}_${chares}_${counter}"
    echo ${JOBNAME}
    aprun -n 16 -N 1 -d 64 -j 1  -r 1 --cc none ./bs_solctra 63488 63488 20000 0.001 1 10000 300000 $JOBNAME 0 +LBDebug 1 +balancer GreedyLB ++ppn62 +pemap 0-61 +commap 62 &
    sleep 1
    let counter++
done

wait

exit 0
#####################################################################

