VECT_FLAGS= -g -O3 -std=c++11 -fopenmp -ffast-math -march=knl -mavx512f -mavx512pf -mavx512er -mavx512cd -fopt-info-vec-optimized=vecreport.dat

#CHARMC=~/./charm-v6.10.2-non-smp-gni-production/gni-crayxc-persistent/bin/charmc $(OPTS) $(VECT_FLAGS)

CHARMC=~/./charm-v6.10.2-smp-gni/gni-crayxc-persistent-smp/bin/charmc $(OPTS) $(VECT_FLAGS)

OBJS = bs_solctra.o kernel.o utils.o

all: bs_solctra

projections: $(OBJS)
	$(CHARMC) -tracemode projections -module CommonLBs -language charm++ -o bs_solctra $(OBJS)

bs_solctra: $(OBJS)
	$(CHARMC) -module CommonLBs -language charm++ -o bs_solctra $(OBJS)

bs_solctra.decl.h: bs_solctra.ci
	$(CHARMC) -E bs_solctra.ci

clean:
	rm -f *.decl.h *.def.h conv-host *.o vecreport.dat bs_solctra charmrun

utils.o: utils.C utils.h
	$(CHARMC) -c utils.C

kernel.o: kernel.C kernel.h
	$(CHARMC) -c kernel.C

bs_solctra.o: bs_solctra.C bs_solctra.decl.h
	$(CHARMC) -c bs_solctra.C

test: all
	./charmrun +p4 ./bs_solctra 32 1024 1000 0.001 1 100 300 777 1 +LBDebug 1 +balancer GreedyLB ++local ++ppn4

analysis: all
	./charmrun +p4 ./bs_solctra 32 1024 1000 0.001 1 100 300 777 +traceroot projections +LBDebug 1 +balancer GreedyLB ++local

restart: all
	./charmrun +p4 ./bs_solctra 32 1024 1000 0.001 1 100 300 777 +restart log +LBDebug 1 +balancer GreedyLB ++local


